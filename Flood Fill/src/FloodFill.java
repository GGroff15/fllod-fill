import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class FloodFill {

	public static void floodFill(Screen screen, Coordenate coordenate, Color previewsColor, Color newColor) {
		if (screen.isInsideScreen(coordenate)) {
			return;
		}

		if (!screen.getColor(coordenate).equals(previewsColor)) {
			return;
		}

		screen.setColor(coordenate, newColor);
		floodFill(screen, new Coordenate(coordenate.x() - 1, coordenate.y()), previewsColor, newColor);
		floodFill(screen, new Coordenate(coordenate.x() + 1, coordenate.y()), previewsColor, newColor);
		floodFill(screen, new Coordenate(coordenate.x(), coordenate.y() - 1), previewsColor, newColor);
		floodFill(screen, new Coordenate(coordenate.x(), coordenate.y() + 1), previewsColor, newColor);
	}

	public static void main(String[] args) {
		int[][] screenMatrix = { { 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1, 0, 0 }, { 1, 0, 0, 1, 1, 0, 1, 1 },
				{ 1, 2, 2, 2, 2, 0, 1, 0 }, { 1, 1, 1, 2, 2, 0, 1, 0 }, { 1, 1, 1, 2, 2, 2, 2, 0 },
				{ 1, 1, 1, 1, 1, 2, 1, 1 }, { 1, 1, 1, 1, 1, 2, 2, 1 } };
		
		var displayRows = 8;
		var displayColumns = 8;

		var screen = new Screen(screenMatrix, displayRows, displayColumns);

		var coordenate = new Coordenate(4, 4);

		var previewsColor = screen.getColor(coordenate);
		var newColor = new Color(3);

		floodFill(screen, coordenate, previewsColor, newColor);

		// Printing the updated screen
		for (int i = 0; i < displayRows; i++) {
			for (int j = 0; j < displayColumns; j++) {
				System.out.print(screen.getColor(new Coordenate(i,j)) + " ");
			}
			System.out.println();
		}
	}
}

record Coordenate(int x, int y) {
}

class Color {
	int value;
	
	Color(int value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return String.valueOf(value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Color other = (Color) obj;
		return value == other.value;
	}
	
}

class Screen {
	private Map<Coordenate, Color> matrix;
	private final int displayRows;
	private final int displayColumns;
	
	public Screen(int [][] matrix, int displayRows, int displayColumns) {
		var screenPositions = new LinkedHashMap<Coordenate, Color>();
		for (int x = 0; x < matrix.length; x++) {
			for (int y = 0; y < matrix[x].length; y++) {
				var color = matrix[x][y];
				screenPositions.put(new Coordenate(x, y), new Color(color));
			}
		}
		this.matrix = new LinkedHashMap<>(screenPositions);
		this.displayRows = displayRows;
		this.displayColumns = displayColumns;
	}
	
	public void setColor(Coordenate coordenate, Color color) {
		matrix.put(coordenate, color);
	}

	public Color getColor(Coordenate coordenate) {
		return matrix.get(coordenate);
	}

	public boolean isInsideScreen(Coordenate coordenate) {
		return coordenate.x() < 0 || coordenate.x() >= displayRows || coordenate.y() < 0 || coordenate.y() >= displayColumns;
	}
}
